require 'spec_helper'
require 'tempfile'
require_relative '../lib/object_storage_backup'
require_relative '../lib/object_storage_azure'

RSpec.describe ObjectStorageBackup do
  let(:name) { 'test-backup' }
  let(:local_tar_path) { '/tmp/backup.tar.gz' }
  let(:remote_bucket) { 'test-bucket' }
  let(:tmp_bucket) { 'tmp-bucket' }
  let(:azure_config_file) { '/path/to/azure.yml' }

  let(:config) do
    <<-CFG
azure_storage_account_name: test
azure_storage_access_key: somekey
    CFG
  end

  let(:config_file) do
    tmp = Tempfile.new('azure-cfg')
    tmp.write(config)
    tmp.flush
    tmp.path
  end

  let(:azure_util) do
    AzureBackupUtil.new(config_file)
  end

  subject(:backup) do
    described_class.new(
      name,
      local_tar_path,
      remote_bucket,
      tmp_bucket,
      'azure',
      's3cmd',
      '',
      '',
      azure_config_file
    )
  end

  before do
    allow(AzureBackupUtil).to receive(:new).and_return(azure_util)
    allow(azure_util).to receive(:sas_token).and_return('test-token')
    allow(FileUtils).to receive(:mkdir_p)
    allow(Dir).to receive(:empty?).and_return(false)
    allow(File).to receive(:exist?).and_return(true)
    allow(Dir).to receive(:glob).and_return(['/tmp/test'])
  end

  describe '#run_cmd' do
    let(:test_cmd) { %w[test command] }
    let(:output) { StringIO.new('command output') }
    let(:status) { double(value: double(exitstatus: 0)) }

    before do
      allow(Open3).to receive(:popen2e).and_return([nil, output, status])
    end

    shared_examples 'command execution' do |expected_env|
      it 'runs command with correct environment' do
        expect(Open3).to receive(:popen2e).with(expected_env, 'test', 'command')
        output, status = backup.send(:run_cmd, test_cmd)
        expect(output).to eq('command output')
        expect(status).to eq(0)
      end
    end

    context 'with azure backend' do
      context 'with static credentials' do
        include_examples 'command execution', {}
      end

      context 'with workload identity' do
        before do
          allow(azure_util).to receive(:static_credentials?).and_return(false)
          allow(azure_util).to receive(:workload_identity?).and_return(true)
        end

        include_examples 'command execution', { 'AZCOPY_AUTO_LOGIN_TYPE' => 'workload' }
      end

      context 'with managed identity' do
        before do
          allow(azure_util).to receive(:static_credentials?).and_return(false)
          allow(azure_util).to receive(:workload_identity?).and_return(false)
        end

        include_examples 'command execution', { 'AZCOPY_AUTO_LOGIN_TYPE' => 'MSI' }
      end
    end

    context 'with non-azure backend' do
      subject(:backup) do
        described_class.new(
          name,
          local_tar_path,
          remote_bucket,
          tmp_bucket,
          'gcs'
        )
      end

      include_examples 'command execution', {}
    end
  end

  describe '#backup' do
    context 'with Azure backend using static credentials' do
      it 'checks if bucket exists and creates backup' do
        expect(backup).to receive(:run_cmd)
          .with(%w[azcopy list https://test.blob.core.windows.net/test-bucket?test-token])
          .and_return(['', 0])

        expect(backup).to receive(:run_cmd)
          .with(%w[azcopy sync https://test.blob.core.windows.net/test-bucket/?test-token /srv/gitlab/tmp/test-backup
                   --output-level essential --exclude-regex=tmp/builds/.*$ --delete-destination=true])
          .and_return(['', 0])

        expect(backup).to receive(:run_cmd)
          .with(%w[tar -cf /tmp/backup.tar.gz -I gzip -C /srv/gitlab/tmp/test-backup .])
          .and_return(['', 0])

        backup.backup
      end

      context 'when bucket does not exist' do
        it 'skips backup' do
          expect(backup).to receive(:run_cmd)
            .with(%w[azcopy list https://test.blob.core.windows.net/test-bucket?test-token])
            .and_return(['', 1])

          expect(backup).not_to receive(:run_cmd)
            .with(%w[azcopy sync https://test.blob.core.windows.net/test-bucket/?test-token /srv/gitlab/tmp/test-backup/
                     --output-level essential --exclude-regex=tmp/builds/.*$ --delete-destination=true])

          backup.backup
        end
      end
    end
  end

  describe '#restore' do
    context 'with static credentials' do
      it 'performs full restore cycle with Azure' do
        # Backup existing
        expect(backup).to receive(:run_cmd)
          .with(%W[azcopy sync
                   https://test.blob.core.windows.net/test-bucket/?test-token
                   https://test.blob.core.windows.net/tmp-bucket/test-backup.#{Time.now.to_i}/?test-token
                   --output-level essential])
          .and_return(['', 0])

        # Cleanup
        expect(backup).to receive(:run_cmd)
          .with(%w[azcopy remove https://test.blob.core.windows.net/test-bucket?test-token
                   --output-level essential --recursive=true])
          .and_return(['', 0])

        # Restore from backup
        expect(backup).to receive(:run_cmd)
          .with(%w[tar -xf /tmp/backup.tar.gz -C /tmp/srv/gitlab/tmp/test-backup])
          .and_return(['', 0])

        # Upload to object storage
        expect(backup).to receive(:run_cmd)
          .with(%w[azcopy sync /tmp/test/ https://test.blob.core.windows.net/test-bucket/test/?test-token
                   --output-level essential])
          .and_return(['', 0])

        backup.restore
      end
    end
  end
end
