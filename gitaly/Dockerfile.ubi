## FINAL IMAGE ##

ARG GITLAB_BASE_IMAGE
ARG UBI_IMAGE=

FROM ${GITLAB_BASE_IMAGE} AS target

FROM ${UBI_IMAGE} AS build

ARG DNF_OPTS
ARG GITLAB_USER=git
ARG UID=1000
ARG DNF_OPTS_ROOT
ARG DNF_INSTALL_ROOT=/install-root

RUN mkdir -p ${DNF_INSTALL_ROOT}
COPY --from=target / ${DNF_INSTALL_ROOT}/
COPY scripts/ ${DNF_INSTALL_ROOT}/scripts/

RUN microdnf update -y \
  && microdnf ${DNF_OPTS} install --nodocs --best --assumeyes --setopt=install_weak_deps=0 shadow-utils \
  && adduser -u ${UID} -m ${GITLAB_USER} -R ${DNF_INSTALL_ROOT}/ \
  && mkdir -p \
  ${DNF_INSTALL_ROOT}/etc/gitaly \
  ${DNF_INSTALL_ROOT}/var/log/gitaly \
  ${DNF_INSTALL_ROOT}/home/${GITLAB_USER}/repositories \
  ${DNF_INSTALL_ROOT}/home/${GITLAB_USER}/custom_hooks \
  ${DNF_INSTALL_ROOT}/srv/gitlab-shell \
  && touch ${DNF_INSTALL_ROOT}/var/log/gitaly/gitaly.log \
  && touch ${DNF_INSTALL_ROOT}/var/log/gitaly/gitlab-shell.log \
  && chown -R ${UID}:0 \
  ${DNF_INSTALL_ROOT}/scripts \
  ${DNF_INSTALL_ROOT}/etc/gitaly \
  ${DNF_INSTALL_ROOT}/var/log/gitaly \
  ${DNF_INSTALL_ROOT}/home/${GITLAB_USER}/repositories \
  ${DNF_INSTALL_ROOT}/srv/gitlab-shell \
  ${DNF_INSTALL_ROOT}/home/${GITLAB_USER} \
  && chmod -R g=u \
  ${DNF_INSTALL_ROOT}/scripts \
  ${DNF_INSTALL_ROOT}/etc/gitaly \
  ${DNF_INSTALL_ROOT}/var/log/gitaly \
  ${DNF_INSTALL_ROOT}/home/${GITLAB_USER}/repositories \
  ${DNF_INSTALL_ROOT}/srv/gitlab-shell \
  ${DNF_INSTALL_ROOT}/home/${GITLAB_USER}

RUN microdnf ${DNF_OPTS} ${DNF_OPTS_ROOT} install --best --assumeyes --nodocs --setopt=install_weak_deps=0 \
  iproute libicu openssh-clients bzip2 gzip \
  && microdnf ${DNF_OPTS_ROOT} clean all \
  && rm -f ${DNF_INSTALL_ROOT}/usr/local/tmp/openssh-server-*.rpm ${DNF_INSTALL_ROOT}/usr/libexec/openssh/ssh-keysign \
  && rm -f ${DNF_INSTALL_ROOT}/var/lib/dnf/history*

ADD gitaly.tar.gz ${DNF_INSTALL_ROOT}/
ADD gitlab-logger.tar.gz ${DNF_INSTALL_ROOT}/usr/local/bin
COPY --chown=${UID}:0 config.toml ${DNF_INSTALL_ROOT}/etc/gitaly/config.toml.tpl

FROM ${GITLAB_BASE_IMAGE}

ARG GITALY_SERVER_VERSION
ARG GITLAB_USER=git
ARG UID=1000
ARG FIPS_MODE=0
ARG DNF_INSTALL_ROOT=/install-root

LABEL source="https://gitlab.com/gitlab-org/build/CNG/-/tree/master/gitaly" \
      name="Gitaly" \
      maintainer="GitLab Distribution Team" \
      vendor="GitLab" \
      version=${GITALY_SERVER_VERSION} \
      release=${GITALY_SERVER_VERSION} \
      summary="Gitaly is a Git RPC service for handling all the git calls made by GitLab." \
      description="Gitaly is a Git RPC service for handling all the git calls made by GitLab."

COPY --from=build  ${DNF_INSTALL_ROOT}/ /

## Hardening: CIS L1 SCAP
RUN --mount=type=bind,rw,from=hardening,source=/,target=/hardening \
    set -ex; for f in /hardening/*.sh; do sh "$f"; done

USER ${UID}

ENV CONFIG_TEMPLATE_DIRECTORY=/etc/gitaly

CMD ["/scripts/process-wrapper"]

VOLUME /var/log/gitaly

HEALTHCHECK --interval=30s --timeout=10s --retries=5 CMD /scripts/healthcheck
