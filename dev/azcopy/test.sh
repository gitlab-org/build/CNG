#!/bin/bash
set -eo pipefail

CONTAINER_URL="${AZURE_CONTAINER_URL:-https://cngazcopy.blob.core.windows.net/gitlab-backups}"

errors=()

# Check for AZURE_SAS_TOKEN
if [ -z "$AZURE_SAS_TOKEN" ]; then
    errors+=("AZURE_SAS_TOKEN is not set.")
fi

# Check for TAG
TAG="${1}"
if [ -z "$TAG" ]; then
    errors+=("TAG is not set.")
fi

# If there are any errors, print them all and exit
if [ ${#errors[@]} -ne 0 ]; then
    echo "Errors:" >&2
    printf ' - %s\n' "${errors[@]}" >&2
    echo "Please set these variables before running the script." >&2
    exit 1
fi

IMAGE=${IMAGE:-registry.gitlab.com/gitlab-renovate-forks/cng/gitlab-toolbox-ee}

echo "${IMAGE}:${TAG}"

docker run --rm -t -i \
  -e "AZURE_SAS_TOKEN=${AZURE_SAS_TOKEN}" \
  -e "CONTAINER_URL=${CONTAINER_URL}" \
  "${IMAGE}:${TAG}" \
  bash -c '    
    cd /tmp # Tests
    echo -e "Checking The tool version:"
    azcopy --version

    echo -e "\nChecking that the bucket is empty:"
    azcopy list "${CONTAINER_URL}?${AZURE_SAS_TOKEN}" --output-type=json | jq -sr '\''.[]'\''

    file_name="1234567890_1234_12_12_12_TEST_gitlab_backup.tar"
    tar -cf ${file_name} -T /dev/null 
    echo -e "copying file ${file_name} to backup bucket ${CONTAINER_URL}/${file_name}:"
    azcopy copy "/tmp/${file_name}" "${CONTAINER_URL}/${file_name}?${AZURE_SAS_TOKEN}" --output-level quiet
    
    echo -e "\nChecking that the file got pushed to the bucket:"
    azcopy list "${CONTAINER_URL}?${AZURE_SAS_TOKEN}" --output-type=json
    
    echo -e "\nRemoving the file:"
    azcopy remove "${CONTAINER_URL}/${filename}?${AZURE_SAS_TOKEN}" --output-level essential

    echo -e "\nChecking that the file got removed:"
    azcopy list "${CONTAINER_URL}?${AZURE_SAS_TOKEN}" --output-type=json
  '
