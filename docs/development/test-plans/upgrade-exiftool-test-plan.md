# `exiftool` component upgrade test plan

You can check the version of `exiftool` and its capabilities by using the
`./dev/exiftool/test.sh` script in this repository.

The script requires:

- Local Docker running locally.
- A successful pipeline triggered by a GitLab team member. 
- `jq` command line tool.

After the pipeline has finished, run from your local repository:

```sh
./dev/exiftool/test.sh $CNG_BRANCH_NAME
```

If your merge request was created from a Renovate bot fork, the branch will be called something like
`renovate/exiftool-exiftool-12.x`. The final image tag will convert `/` and `.` to
`-` (for example, `renovate-exiftool-exiftool-12-x`). In this case, you run:

```sh
./exiftool-test.sh renovate-exiftool-exiftool-12-x
```

Use the output of the script to verify:

- The output version is expected.
- Each test output matches the expectations in the test comments. Refer to the `./dev/exiftool/test.sh` file.
- Each image present in `dev/exiftool/output` in your local environment should open correctly.
