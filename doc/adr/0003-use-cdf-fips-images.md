# 1. Record architecture decisions

Date: 2024-11-06

## Status

Accepted

## Context

As part of FIPS compliance for the images produced by the Cloud Native GitLab containers project,
we need to ensure that we are verifying the contents of the images in regards to FIPS / NIST CMVP.

## Decision

We will make use of the [Container Dependency Finder](https://gitlab.com/gitlab-org/cloud-native/container-dependencies-finder)
and the [CI/CD component](https://docs.gitlab.com/ee/ci/components/) for `rpm-verify-fips`. For more information, see
[the documentation](https://gitlab.com/gitlab-org/cloud-native/container-dependencies-finder#ci-components).
The implementation was constructed so that it will safely operate on public forks of this project.

## Consequences

When building FIPS images, we have supplemental jobs included in CI/CD pipelines.
These jobs will inform us of contents and packages related to NIST CMVP, in compliance with FIPS
standards. Failures of these jobs are not necessarily hard failures, but should lead to an
examination of any finding to ensure that any deviation is intentional.
